<?php
$host = '';
$db   = '';
$user = '';
$pass = '';
$port = '3306';

if (is_file('.config.php'))require('.config.php');

try
    {
        $bdd = new PDO("mysql:host=$host;port=$port;dbname=$db;charset=utf8", $user, $pass);
    }
catch (Exception $e)
    {
            die('Erreur : ' . $e->getMessage());
    }
