<?php

require('connect.php');
require_once('header.php');

if (!$user) {
    header('Location: login.php');
    exit;
}

if(!empty($_GET['user'])) {
    // update
    if (!empty($_POST['login'])) {
        $query = $bdd->prepare('SELECT * FROM user WHERE login=:login');
        $query->execute([
           'login' => $_POST['login']
        ]);
        $dbuser = $query->fetch();
        if (!$dbuser) {
            $query = $bdd->prepare('UPDATE user SET login=:login WHERE id=:id');
            $query->execute([
                'login' => htmlspecialchars($_POST['login']),
                'id' => $_GET['user']
            ]);

            header('Location: user-update.php?user='.$_GET['user'].'&modify=ok');
            exit;
        } else {
            header('Location: user-update.php?user='.$_GET['user']);
            exit;
        }
    }
    // update

    $query = $bdd->prepare('SELECT * FROM user WHERE id=:user');
    $query->execute([
        'user' => $_GET['user']
    ]);

    $data = $query->fetch();
} else {
    header('Location: admin_users.php');
    exit;
}

?>

<div class="container">
    <h1>Mise à jour utilisateur</h1>
    <?php if (!empty($_GET['modify'])) {
        echo '<div class="text-valid">Modification effectuée</div><br>';
    }?>
    <form action="#" method="post">
      <div class="form-input">
        <label for="login">Identifiant: </label>
        <input type="text" name="login" value="<?php echo $data['login'] ?>">
      </div>
      <input type="submit">
    </form>
</div>



