<?php
// CRUD
require('connect.php');
require_once('header.php');
if (!$user) {
    header('Location: login.php');
    exit;
}

// Read
$query = $bdd->prepare('SELECT * FROM user');
$query->execute();

$data = $query->fetchAll();
// Read

?>
<div class="container">
  <h1>Liste des utilisateurs</h1>
  <a href="user-create.php" class="btn">Créer un utilisateur</a><br><br>
  <div class="list">
    <table>
      <thead>
      <tr>
        <th>ID</th>
        <th>login</th>
        <th>password hash</th>
        <th></th>
      </tr>
      </thead>
        <?php
        foreach($data as $dbuser){
            ?>
          <tr>
            <td><?php echo $dbuser['id'] ?></td>
            <td><?php echo $dbuser['login'] ?></td>
            <td><?php echo $dbuser['pass'] ?></td>
            <td><a href="user-update.php?user=<?php echo $dbuser['id'] ?>"><i class="fa-solid fa-pen-to-square"></i></a>
                <?php if($dbuser['id'] !== $_SESSION['user']){ ?>
                <a href="user-delete.php?user=<?php echo $dbuser['id'] ?>"><i class="fa-solid fa-trash-can"></i>
                </a><?php } ?>
            </td>
          </tr>
            <?php
        }
        ?>
    </table>
  </div>
</div>
