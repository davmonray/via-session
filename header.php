<?php

$user = null;
  if (session_status() !== PHP_SESSION_ACTIVE) session_start();
  if (!empty($_SESSION['user'])) {
    $user = $_SESSION['user'];
  }

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css">
  <link rel="stylesheet" href="style.css">
  <title>Document</title>
</head>
<body>
<div class="header">
    <div class="logo">Via-Formation</div>
    <div class="navbar">
        <ul>
          <!--  Uniquement si un utilisateur est connecté-->
            <?php if ($user){ ?>
              <li><a href="#">Rédaction article</a></li>
              <li><a href="admin-users.php">Gestion des utilisateurs</a></li>
            <?php } ?>
          <!-- Uniquement si un utilisateur est connecté-->
            <li><a href="#">Mes projets</a></li>
            <li><a href="#">Mon parcours</a></li>
            <li><a href="#">Me contacter</a></li>
        </ul>
    </div>
    <div class="connect">
        <?php if (!$user){ ?><a href="login.php">Se connecter</a>
        <?php } else { ?>
            &nbsp;&nbsp;&nbsp;
            <a href="logout.php">Se déconnecter</a>
        <?php } ?>
        <a href="user-create.php" class="btn">Se créer un compte</a>
    </div>
</div>
