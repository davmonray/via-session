<?php
session_start();
require('connect.php');

if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $query = $bdd->prepare('SELECT id, pass FROM user WHERE login = :login');
    $query->execute([
       'login' => $_POST['login']
    ]);
    $data = $query->fetch();
    if ($data) {
        if (password_verify($_POST['password'], $data['pass'])){
            $_SESSION['user'] = $data['id'];
            header('Location: admin-users.php');
        }
    }
}
require_once('header.php');

?>

<div class="form-login">
  <h1>Se connecter</h1>
    <form action="#" method="post">
      <div class="form-input">
        <label for="login">Login: </label>
        <input type="text" name="login" id="login">
      </div>
      <div class="form-input">
        <label for="password">Mot de passe: </label>
        <input type="password" name="password" id="password">
      </div>
      <input type="submit" value="Se connecter">
    </form>
</div>

</body>
</html>
