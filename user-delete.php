<?php

require('connect.php');
require_once('header.php');

if (!$user) {
    header('Location: login.php');
    exit;
}

// Delete
if(!empty($_GET['user'])) {
    if ((int)$_GET['user'] !== (int)$_SESSION['user']) {
        $query = $bdd->prepare('DELETE FROM user WHERE id=:id');
        $query->execute([
           'id' => $_GET['user']
        ]);
    }
}
// Delete

header('Location: admin-users.php');
exit;
