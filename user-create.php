<?php

require('connect.php');
require_once('header.php');

// Create
if (!empty($_POST['login']) && !empty($_POST['password'])) {
    $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);

    $query = $bdd->prepare('INSERT INTO user (login, pass) VALUES (:login, :password)');
    $query->execute([
        'login' => htmlspecialchars($_POST['login']),
        'password' => $hash
    ]);

    header('Location: admin-users.php');
    exit;
}
// Create

?>
<div class="form-login">
  <h1>Création de compte</h1>
    <form action="#" method="post">
      <div class="form-input">
        <label for="login">Identifiant: </label>
        <input type="text" name="login" id="login" placeholder="Entrez un identifiant">
      </div>
      <div class="form-input">
        <label for="password">Mot de passe: </label>
        <input type="password" name="password" id="password" placeholder="Entrez un mot de passe">
      </div>
      <input type="submit">
    </form>
</div>
