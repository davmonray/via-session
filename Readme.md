## config ###
- Create a file '.config.php' with these content:

```php
<?php

$host = host_url;
$db   = database_name;
$user = database_user;
$pass = database_password;
$port = db_port;
```
